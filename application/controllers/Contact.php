<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Contact extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('User_model', 'user');
        $this->load->model('Contact_model', 'contact');
        $this->load->model('Admin_model', 'admin');
    }

    public function index()
    {   
        $data['title'] = 'Contact Management';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['contacts'] = $this->contact->getContacts($this->session->userdata('id'));

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('contact/index', $data);
        $this->load->view('templates/footer', $data);
    }

    public function details($contact_id)
    {   
        $data['title'] = 'Detailed Contact Information';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        // Increment number of views
        $plus = $this->db->get_where('user_contacts', ['id' => $contact_id])->row()->no_views;
        $inc = $plus + 1;
        $this->db->where('id', $contact_id);
        $query = $this->db->update('user_contacts', ['no_views' => $inc]);

        $data['contacts'] = $this->db->get_where('user_contacts', ['id' => $contact_id])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('contact/detail-view', $data);
        $this->load->view('templates/footer', $data);
    }

    public function add()
    {
        $data['title'] = 'Add Contact';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->form_validation->set_rules('fname', 'First Name', 'trim|required');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('mobile', 'Mobile Number', 'trim|required|numeric');
        $this->form_validation->set_rules('landline', 'Landline Number', 'trim|required|numeric');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('contact/add-contact', $data);
            $this->load->view('templates/footer');
        } else {
            $insert = array(
                'user_id' => $this->session->userdata('id'),
                'first_name' => $this->input->post('fname'),
                'middle_name' => $this->input->post('mname'),
                'last_name' => $this->input->post('lname'),
                'email' => $this->input->post('email'),
                'mobile' => $this->input->post('mobile'),
                'landline' => $this->input->post('landline'),
                'notes' => $this->input->post('notes'),
                'date_created' => time()
            );
            if($this->db->insert('user_contacts', $insert)){
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New contact added!</div>');
                redirect('contact');
            }
            else{
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Error in ading the contact!</div>');
                redirect('contact');
            }
        }
    }

    public function edit($contact_id)
    {
        $data['title'] = 'Edit Contact';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['contacts'] = $this->db->get_where('user_contacts', ['id' => $contact_id])->row_array();

        $this->form_validation->set_rules('fname', 'First Name', 'required');
        $this->form_validation->set_rules('lname', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|numeric');
        $this->form_validation->set_rules('landline', 'Landline Number', 'required|numeric');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('contact/edit-contact', $data);
            $this->load->view('templates/footer');
        } else {
            $update = array(
                'first_name' => $this->input->post('fname'),
                'middle_name' => $this->input->post('mname'),
                'last_name' => $this->input->post('lname'),
                'email' => $this->input->post('email'),
                'mobile' => $this->input->post('mobile'),
                'landline' => $this->input->post('landline'),
                'notes' => $this->input->post('notes'),
                'date_modified' => time()
            );
            $getContact = $this->db->get_where('user_contacts', ['id' => $contact_id]);

            if ($getContact->num_rows() == 1) {
                $this->db->where('id', $contact_id);
                $this->db->update('user_contacts', $update);
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Contact updated successfully!</div>');
                redirect('contact');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Error in updating the contact!</div>');
                redirect('contact/edit/'.$contact_id);
            }
        }
    }

    public function delete($contact_id)
    {
        $contacts = $this->db->get_where('user_contacts', ['id' => $contact_id])->row_array();

        $this->db->delete('user_contacts', ['id' => $contact_id]);
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Contact is deleted!</div>');
        redirect('contact');
    }
}
