<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Contact_model extends CI_Model
{
    public function getContacts($id)
    {   
        $this->db->where("user_id", $id);
        $query = $this->db->get("user_contacts");
        return $query->result_array();
    }
}
