<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="row">
        <div class="col-lg-8">
            <?= $this->session->flashdata('message'); ?>
            <?= form_open('contact/add/'); ?>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">First Name *</label>
                <div class="col-sm-9">
                    <input type="text" name="fname" id="fname" class="form-control">
                    <?= form_error('fname', '<small class="text-danger pl-3">', '</small>') ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Middle Name</label>
                <div class="col-sm-9">
                    <input type="text" name="mname" id="mname" class="form-control">
                    <?= form_error('mname', '<small class="text-danger pl-3">', '</small>') ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Last Name *</label>
                <div class="col-sm-9">
                    <input type="text" name="lname" id="lname" class="form-control">
                    <?= form_error('lname', '<small class="text-danger pl-3">', '</small>') ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Email *</label>
                <div class="col-sm-9">
                    <input type="text" name="email" id="email" class="form-control">
                    <?= form_error('email', '<small class="text-danger pl-3">', '</small>') ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Mobile Number *</label>
                <div class="col-sm-9">
                    <input type="text" name="mobile" id="mobile" class="form-control">
                    <?= form_error('mobile', '<small class="text-danger pl-3">', '</small>') ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Landline Number *</label>
                <div class="col-sm-9">
                    <input type="text" name="landline" id="landline" class="form-control">
                    <?= form_error('landline', '<small class="text-danger pl-3">', '</small>') ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Notes</label>
                <div class="col-sm-9">
                    <textarea name="notes" id="notes" class="form-control"></textarea>
                    <?= form_error('notes', '<small class="text-danger pl-3">', '</small>') ?>
                </div>
            </div>
            <br/>
            <div class="form-group row">
                <div class="col-sm-10">
                    <a href="<?= base_url('contact'); ?>" class="btn btn-secondary">Back</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->