<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <div class="row">
        <div class="col-lg-6">
            <?= $this->session->flashdata('message'); ?>
        </div>
    </div>
    <div class="card mb-3 col-lg-8">
        <div class="row no-gutters">
            <div class="col-lg-12">
                <div class="card-body">
                    <p class="card-text">
                        First Name : <?= $contacts['first_name']; ?>
                    </p>
                    <p class="card-text">
                        Middle Name : <?= $contacts['middle_name']; ?>
                    </p>
                    <p class="card-text">
                        Last Name : <?= $contacts['last_name']; ?>
                    </p>
                    <p class="card-text">
                        Email : <?= $contacts['email']; ?>
                    </p>
                    <p class="card-text">
                        Mobile Number : <?= $contacts['mobile']; ?>
                    </p>
                    <p class="card-text">
                        Landline Number : <?= $contacts['landline']; ?>
                    </p>
                    <p class="card-text">
                        Notes : <?= $contacts['notes']; ?>
                    </p>
                    <p class="card-text">
                        Total Views : <?= $contacts['no_views']; ?>
                    </p>
                    <p class="card-text">
                        <?php if($contacts['date_modified'] === 0) { ?>
                        <small><strong class="text-muted"> - Last edited on <?= date('d F Y', $contacts['date_modified']); ?>
                        </strong></small>
                        <?php } else { ?>
                        <small><strong class="text-muted"> - Last edited (N/A)
                        </strong></small>
                        <?php } ?>
                    </p>
                    <p class="card-text">
                        <small class="text-muted"> Member since <?= date('d F Y', $contacts['date_created']); ?>
                        </small>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <a href="<?= base_url('contact'); ?>" class="btn btn-primary">Go Back</a>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->