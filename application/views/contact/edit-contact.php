<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="row">
        <div class="col-lg-8">
            <?= $this->session->flashdata('message'); ?>
            <?= form_open('contact/edit/'.$contacts['id']); ?>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">First Name</label>
                <div class="col-sm-9">
                    <input type="text" name="fname" id="fname" class="form-control" value="<?= $contacts['first_name']; ?>">
                    <?= form_error('fname', '<small class="text-danger pl-3">', '</small>') ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Middle Name</label>
                <div class="col-sm-9">
                    <input type="text" name="mname" id="mname" class="form-control" value="<?= $contacts['middle_name']; ?>">
                    <?= form_error('mname', '<small class="text-danger pl-3">', '</small>') ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Last Name</label>
                <div class="col-sm-9">
                    <input type="text" name="lname" id="lname" class="form-control" value="<?= $contacts['last_name']; ?>">
                    <?= form_error('lname', '<small class="text-danger pl-3">', '</small>') ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Email</label>
                <div class="col-sm-9">
                    <input type="text" name="email" id="email" class="form-control" value="<?= $contacts['email']; ?>">
                    <?= form_error('email', '<small class="text-danger pl-3">', '</small>') ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Mobile Number</label>
                <div class="col-sm-9">
                    <input type="text" name="mobile" id="mobile" class="form-control" value="<?= $contacts['mobile']; ?>">
                    <?= form_error('mobile', '<small class="text-danger pl-3">', '</small>') ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Landline Number</label>
                <div class="col-sm-9">
                    <input type="text" name="landline" id="landline" class="form-control" value="<?= $contacts['landline']; ?>">
                    <?= form_error('landline', '<small class="text-danger pl-3">', '</small>') ?>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Notes</label>
                <div class="col-sm-9">
                    <textarea name="notes" id="notes" class="form-control">
                        <?= $contacts['notes']; ?>
                    </textarea>
                    <?= form_error('notes', '<small class="text-danger pl-3">', '</small>') ?>
                </div>
            </div>
            <br/>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </div>

            </form>

        </div>
    </div>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->