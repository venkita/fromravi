<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <?= form_error('contact', '<div class="w-80 alert alert-danger" role="alert" style="float: left;">', '</div>'); ?>
            <?= $this->session->flashdata('message'); ?>
            <a href="<?= base_url("contact/add/") ?>" class="btn btn-primary" style="float: right;">Add New Contact</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th scope="col">Sl. No.</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Mobile</th>
                            <th scope="col">Landline</th>
                            <th scope="col">Notes</th>
                            <th scope="col">Date Created</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($contacts as $c) : ?>
                            <tr>
                                <td scope="row"><?= $i; ?></td>
                                <td scope="row">
                                <?= 
                                    $c['first_name']." ".$c['middle_name']." ".$c['last_name']; 
                                ?>        
                                </td>
                                <td scope="row"><?= $c['email']; ?></td>
                                <td scope="row"><?= $c['mobile']; ?></td>
                                <td scope="row"><?= $c['landline']; ?></td>
                                <td scope="row"><?= $c['notes']; ?></td>
                                <td scope="row"><?= date('d F Y', $c['date_created']); ?></td>
                                <td scope="row">
                                    <a href="<?= base_url('contact/details/'.$c['id']); ?>" class="badge badge-primary">Details</a>
                                    <a href="<?= base_url('contact/edit/'.$c['id']); ?>" class="badge badge-success">Edit</a>
                                    <a href="<?= base_url('contact/delete/'.$c['id']); ?>" class="badge badge-danger" data-toggle="modal" data-target="#deleteContact">Delete</a>
                                </td>
                            </tr>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<div class="modal fade" id="deleteContact" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Contact</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Are you sure want to delete this contact?</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <?php 
                    if(!empty($contacts)){
                ?>
                <a class="btn btn-primary" href="<?= base_url('contact/delete/').$contacts[0]['id']; ?>">Delete</a>
                <?php } else { ?>
                <a class="btn btn-primary" href="#">Delete</a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>